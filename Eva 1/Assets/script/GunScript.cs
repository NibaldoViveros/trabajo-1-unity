﻿
using UnityEngine;

public class GunScript : MonoBehaviour
{
    public float Damage = 10f;
    public float Range = 100f;
    public float fireRate = 15f;
    public float ImpactForce = 30f;

    public Camera fpscam;
    public ParticleSystem Flash;
    //public GameObject impactFX;
    public AudioSource GunFX;

    private float nextTimeToFire = 0f;


    private void Start()
    {
        GunFX = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1") && Time.time >= nextTimeToFire)
        {
            GunFX.Play();
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();
        }
        

        void Shoot()
        {
            Flash.Play();

            RaycastHit hit;
            if (Physics.Raycast(fpscam.transform.position, fpscam.transform.forward, out hit, Range))
            {
                Debug.Log(hit.transform.name);

                Target target = hit.transform.GetComponent<Target>();
                if (target != null)
                {
                    target.TakeDamage(Damage);
                }

                if (hit.rigidbody != null)
                {
                    hit.rigidbody.AddForce(-hit.normal * ImpactForce);
                }

                //GameObject Impact = Instantiate(impactFX, hit.point, Quaternion.LookRotation(hit.normal));
                //if(Impact != null)
                //{
                //    Destroy(Impact, 2f);
                //}
                
            }
        }
    }
}
