﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controler : MonoBehaviour
{
    public float movementSpeed = 2f;
    float timer;

    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if (timer <= 3f)
        {
            transform.Translate(Vector3.right * movementSpeed * Time.deltaTime);
        }
        else
        {
            if (timer <=6f)
            {
                transform.Translate(Vector3.left * movementSpeed * Time.deltaTime);

            }
            else
            {
                timer = 0;
            }
        }

        
    }
}
