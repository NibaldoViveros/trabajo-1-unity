﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuntoSpawner : MonoBehaviour
{
    public int Dificultad = 1;
    public GameObject puntoPrefab;
 
    void Start()
    {
        for (int i = 0; i < 5; i++)
        {
            float x = Random.Range(-11, 5);
            float y = Random.Range(1,4);
            float z = Random.Range(7, 20);
            Vector3 position = new Vector3(x, y, z);
            Quaternion rotation = new Quaternion();
            Instantiate(puntoPrefab, position, rotation);
            
        }
    }

    void Update()
    {


    }
}
