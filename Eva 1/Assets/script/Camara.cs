﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour
{
    public float mouseSensitivity = 80;
    public Transform playerbody;
    float xRotacion = 0;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotacion -= mouseY;
        xRotacion = Mathf.Clamp(xRotacion,-90f,90);

        transform.localRotation = Quaternion.Euler(xRotacion,0f,0f);

        playerbody.Rotate(Vector3.up*mouseX);
    }
}
